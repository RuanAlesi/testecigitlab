***Settings***
Library     RequestsLibrary
Library     Collections

***Variables***
${URL}      http://superheroapi.com/api/4385634404811012

***Keywords***
### DADO
Dado que esteja conectado na SuperHeroAPI
    Create Session      sessao      ${URL}      verify=true


### QUANDO
Quando requisitar o histórico do super herói "${nome_heroi}"
    ${pesquisa_id}          Get Request         sessao      /search/${nome_heroi}
    ${id}                   Pesquisa heroID     ${pesquisa_id}      ${nome_heroi}
    ${resposta}             Get Request         sessao      /${id}
    Set Test Variable       ${resposta}

Quando requisitar as estatísticas de poder dos super heróis "${nome_heroi_1}" e "${nome_heroi_2}"
    ${req_heroi_1}          Get Request         sessao      /search/${nome_heroi_1}
    ${req_heroi_2}          Get Request         sessao      /search/${nome_heroi_2}
    ${id_heroi_1}           Pesquisa heroID     ${req_heroi_1}      ${nome_heroi_1}
    ${id_heroi_2}           Pesquisa heroID     ${req_heroi_2}      ${nome_heroi_2}
    ${pwrstt_hero_1}        Get Request         sessao      /${id_heroi_1}/powerstats
    ${pwrstt_hero_2}        Get Request         sessao      /${id_heroi_2}/powerstats
    Set Test Variable       ${pwrstt_hero_1}
    Set Test Variable       ${pwrstt_hero_2}
    Set Test Variable       ${req_heroi_1} 
    Set Test Variable       ${req_heroi_2} 

### ENTÃO
Então deve ser retornado que a sua inteligência é superior a "${inteligencia}"
    log                         ${resposta.json()}
    ${pwrstt_intelligence}      Get from Dictionary       ${resposta.json()['powerstats']}    intelligence
    Convert to Number           ${pwrstt_intelligence}
    Should Be True              ${pwrstt_intelligence} > ${inteligencia}

E deve ser retornado que o seu verdadeiro nome é ser "${nome}"
    Dictionary Should Contain Item      ${resposta.json()['biography']}     full-name       ${nome}

E deve ser retornado que é afiliado do grupo "${grupo}"
    Should Contain      ${resposta.json()['connections']['group-affiliation']}    ${grupo}

Então deve ser retornado que o mais inteligente é o herói "${nome_heroi}"
    ${nome_heroi_campeao}       Heroi Campeão       ${pwrstt_hero_1}      ${pwrstt_hero_2}       intelligence
    Should Be Equal     ${nome_heroi_campeao}       ${nome_heroi}

E deve ser retornado que o mais rápido é o herói "${nome_heroi}"
    ${nome_heroi_campeao}       Heroi Campeão       ${pwrstt_hero_1}      ${pwrstt_hero_2}       speed
    Should Be Equal     ${nome_heroi_campeao}       ${nome_heroi}

E deve ser retornado que o mais forte é o herói "${nome_heroi}"
    ${nome_heroi_campeao}       Heroi Campeão       ${pwrstt_hero_1}      ${pwrstt_hero_2}       strength
    Should Be Equal     ${nome_heroi_campeao}       ${nome_heroi}
    
### MINHAS KEYWORDS
Pesquisa heroID
    [Arguments]         ${pesquisa}         ${nome_heroi}
    ${id_heroi}         Get from Dictionary       ${pesquisa.json()['results']['name'='${nome_heroi}']}    id
    [Return]            ${id_heroi}

Get Nome do Heroi
    [Arguments]     ${req_heroi}
    Log             ${req_heroi.json()}
    ${nome}         Get from Dictionary         ${req_heroi.json()}       name
    [Return]        ${nome}

Heroi Campeão
    [Arguments]     ${heroi1}       ${heroi2}       ${criterio}
    ${valor_heroi_1}     Get from Dictionary        ${heroi1.json()}      ${criterio}
    ${valor_heroi_2}     Get from Dictionary        ${heroi2.json()}      ${criterio}
    ${heroi_campeao}     Set Variable If
    ...                  ${valor_heroi_1} > ${valor_heroi_2}    ${heroi1}
    ...                  ${valor_heroi_1} < ${valor_heroi_2}    ${heroi2}
    ${nome_heroi_campeao}   Get Nome do Heroi       ${heroi_campeao}
    [Return]        ${nome_heroi_campeao}
